﻿using ConsoleMinify.Interfaces;
using System;
using System.Text;

namespace ConsoleMinify
{
  class Program
  {
    const string OVERWRITE_ARGUMENT = "-overwrite";
    private static ILogger _logger;

    static void Main(string[] args)
    {
      _logger = new ConsoleLogger();
      if (!CheckArgumentsValid(args))
      {
        DisplayUsage();
        return;
      }

      string sourceFile = args[0];
      string destFile = args[1];
      bool overwrite = args.Length == 3; //By now we know if there are three args, the third one is /overwrite

      var minifier = new Minifier(_logger, sourceFile, destFile);
      try
      {
        minifier.Minify(overwrite);
        _logger.LogInformation(string.Format("Minified {0}", destFile));
      } catch (Exception ex)
      {
        _logger.LogError(ex.Message);
      }
    }

    private static bool CheckArgumentsValid(string[] args)
    {
      if (args.Length != 2 && args.Length != 3)
        return false;
      if (args.Length == 2)
        return true;

      return args.Length == 3 && args[2].ToLower() == OVERWRITE_ARGUMENT;
    }

    private static void DisplayUsage()
    {
      StringBuilder message = new StringBuilder(3);
      message.Append(string.Format("Usage: ConsoleMinify <src> <dest> [{0}]\r\n", OVERWRITE_ARGUMENT));
      message.Append("\r\n");
      message.Append(string.Format("Use {0} to force this app to overwrite the dest file if it exists\r\n", OVERWRITE_ARGUMENT));
      _logger.LogInformation(message.ToString());
    }
  }
}
