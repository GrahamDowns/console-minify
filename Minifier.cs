﻿using ConsoleMinify.Interfaces;
using System.IO;
using System.Text.RegularExpressions;

namespace ConsoleMinify
{
  public class Minifier : IMinifier
  {
    private readonly ILogger _logger;
    private readonly string _sourceFile;
    private readonly string _destinationFile;

    public Minifier(ILogger logger, string sourceFile, string destinationFile)
    {
      _logger = logger;
      _sourceFile = sourceFile;
      _destinationFile = destinationFile;
    }

    public void Minify(bool overwriteDestination)
    {
      string text = LoadFile();
      StripHtmlComments(ref text);
      //TODO Strip JavaScript/CSS comments (When I have a need)
      StripCrLf(ref text);
      StripTabs(ref text);
      CondenseSpaces(ref text);
      SaveFile(text, overwriteDestination);
    }

    private void StripHtmlComments(ref string text)
    {
      text = Regex.Replace(text, "<!--.*?-->", "", RegexOptions.Singleline);
    }

    private void SaveFile(string text, bool overwriteDestination)
    {
      if (!overwriteDestination && File.Exists(_destinationFile))
        throw new FileLoadException(string.Format("The file {0} already exists. To force it to overwrite, use the overwrite option.", _destinationFile));
      File.WriteAllText(_destinationFile, text);
    }

    private void CondenseSpaces(ref string text)
    {
      while (text.Contains("  "))
      {
        text = text.Replace("  ", " ");
      }
    }

    private void StripTabs(ref string text)
    {
      text = text.Replace("\t", " ");
    }

    private void StripCrLf(ref string text)
    {
      text = text.Replace("\r", " ").Replace("\n", " ");
    }

    private string LoadFile()
    {
      return File.ReadAllText(_sourceFile);
    }
  }
}
