﻿namespace ConsoleMinify.Interfaces
{
  //Note: If you want to one day make this app log to an event log, you may want to add a new signature
  //for LogWarning, and possibly change the signatures to accept an Event ID as well.
  //For this case, the only place we're going to be logging is the Console.
  public interface ILogger
  {
    void LogInformation(string message);
    void LogError(string message);
  }
}
