﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleMinify.Interfaces
{
  public interface IMinifier
  {
    void Minify(bool overwriteDestination);
  }
}
