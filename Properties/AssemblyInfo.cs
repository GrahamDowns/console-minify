﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("Console Minify")]
[assembly: AssemblyDescription("Runs basic minification logic on a file you pass in")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("Console Minify")]
[assembly: AssemblyCopyright("Copyright © Graham Downs 2017")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]
[assembly: ComVisible(false)]
[assembly: Guid("9ae80633-6b6b-4f90-8883-64f0292fcd98")]
[assembly: AssemblyVersion("1.0.1.1")]
[assembly: AssemblyFileVersion("1.0.1.1")]
