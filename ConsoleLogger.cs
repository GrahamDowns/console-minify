﻿using ConsoleMinify.Interfaces;
using System;

namespace ConsoleMinify
{
  public class ConsoleLogger : ILogger
  {
    public void LogInformation(string message)
    {
      Console.WriteLine(message);
    }

    public void LogError(string message)
    {
      Console.WriteLine(string.Format("[ERROR] {0}", message));
    }
  }
}
